﻿using System.Collections.Generic;

namespace CodeTranslation
{
    public interface IScore
    {
        int Multiplier { get; }
        int Score { get; }
        void UpdateScore(List<Obstacle> obstacles);
        void SaveScore(string playername);
    }
}