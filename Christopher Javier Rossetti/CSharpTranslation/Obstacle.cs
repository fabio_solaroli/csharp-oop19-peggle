﻿namespace CodeTranslation
{
    public enum ObstacleBehavior
    {
        RED = 100,
        BLUE = 10,
        GREEN = 10,
        PURPLE = 500,
    }
    public class Obstacle
    {
        public ObstacleBehavior Color { get; set; }

        public Obstacle()
        {
            this.Color = ObstacleBehavior.BLUE;
        }
    }
}
