﻿using System;
using System.Collections.Generic;
using System.IO;
using static System.Environment;

namespace CodeTranslation
{
    public class ScoreImpl : IScore
    {
        private const int MULTIPLIER_X2 = 2;
        private const int MULTIPLIER_X3 = 3;
        private const int MULTIPLIER_X5 = 5;
        private const int MULTIPLIER_X10 = 10;
        private const int LV1 = 10;
        private const int LV2 = 15;
        private const int LV3 = 19;
        private const int LV4 = 22;

        private readonly FileInfo file = new FileInfo(Environment.GetFolderPath(SpecialFolder.UserProfile) + "\\tegole\\leaderboard\\leaderboard.txt");
        private readonly List<KeyValuePair<string, int>> scorelist;
        private int tempScore;
        private int redObstaclesHit;

        public int Score { get; private set; }
        public int Multiplier { get; private set; }

        public ScoreImpl()
        {
            scorelist = new List<KeyValuePair<string, int>>();
            this.Score = 0;
            this.tempScore = 0;
            this.redObstaclesHit = 0;
            this.Multiplier = 1;
        }

        public void UpdateScore(List<Obstacle> obstacles)
        {
            obstacles.ForEach(o =>
            {
                if (o.Color == ObstacleBehavior.RED)
                {
                    this.redObstaclesHit++;
                    this.SetMultiplier();
                }
                this.tempScore += (int)o.Color * this.Multiplier;
            });

            if (obstacles.Count > 0)
            {
                this.tempScore *= obstacles.Count;
            }
            this.Score += this.tempScore;
            this.tempScore = 0;
        }

        private void SetMultiplier()
        {
            if (this.redObstaclesHit >= LV4)
            {
                this.Multiplier = MULTIPLIER_X10;
            }
            else if (this.redObstaclesHit >= LV3)
            {
                this.Multiplier = MULTIPLIER_X5;
            }
            else if (this.redObstaclesHit >= LV2)
            {
                this.Multiplier = MULTIPLIER_X3;
            }
            else if (this.redObstaclesHit >= LV1)
            {
                this.Multiplier = MULTIPLIER_X2;
            }
        }

        public void SaveScore(string playername)
        {
            this.scorelist.Add(new KeyValuePair<string, int>(playername, this.Score));

            if (!file.Exists)
            {
                Directory.CreateDirectory(file.DirectoryName);
                file.Create().Close();

            }
            else
            {
                this.SortLeaderboard();
            }

            using StreamWriter sw = file.CreateText();
            scorelist.ForEach(o =>
            {
                sw.WriteLine(o.Key + " " + o.Value);
            });
        }

        private void SortLeaderboard()
        {
            foreach (string line in File.ReadAllLines(file.FullName))
            {
                string[] array = line.Split(" ");
                this.scorelist.Add(new KeyValuePair<string, int>(array[0], int.Parse(array[1])));
            }

            scorelist.Sort((o1, o2) => o2.Value - o1.Value);
        }
    }
}
