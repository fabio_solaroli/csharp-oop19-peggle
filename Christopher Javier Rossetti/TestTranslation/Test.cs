using CodeTranslation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace TestTranslation
{
    [TestClass]
    public class Test
    {
        private const int MULTIPLIER_X2 = 2;
        private const int MULTIPLIER_X3 = 3;
        private const int MULTIPLIER_X5 = 5;
        private const int MULTIPLIER_X10 = 10;

        [TestMethod]
        public void TestUpdateScore()
        {
            // Adds a blue obstacle and tests if the score and multiplier are correct
            IScore score = new ScoreImpl();
            score.UpdateScore(new List<Obstacle> { new Obstacle() });
            Assert.AreEqual(1, score.Multiplier);
            Assert.AreEqual((int)ObstacleBehavior.BLUE, score.Score);

            // Adds 10 red obstacles and tests if the multiplier is correct
            List<Obstacle> redlist = Enumerable.Range(0, 10).Select(i => new Obstacle()).ToList();
            redlist.ForEach(o => o.Color = ObstacleBehavior.RED);
            score.UpdateScore(redlist);
            Assert.AreEqual(MULTIPLIER_X2, score.Multiplier);

            // Adds red obstacles to a total of 15 and tests if the multiplier is correct
            redlist.Clear();
            redlist = Enumerable.Range(0, 5).Select(i => new Obstacle()).ToList();
            redlist.ForEach(o => o.Color = ObstacleBehavior.RED);
            score.UpdateScore(redlist);
            Assert.AreEqual(MULTIPLIER_X3, score.Multiplier);

            // Adds red obstacles to a total of 19 and tests if the multiplier is correct
            redlist.Clear();
            redlist = Enumerable.Range(0, 4).Select(i => new Obstacle()).ToList();
            redlist.ForEach(o => o.Color = ObstacleBehavior.RED);
            score.UpdateScore(redlist);
            Assert.AreEqual(MULTIPLIER_X5, score.Multiplier);

            // Adds red obstacles to a total of 22 and tests if the multiplier is correct
            redlist.Clear();
            redlist = Enumerable.Range(0, 3).Select(i => new Obstacle()).ToList();
            redlist.ForEach(o => o.Color = ObstacleBehavior.RED);
            score.UpdateScore(redlist);
            Assert.AreEqual(MULTIPLIER_X10, score.Multiplier);

            // Adds a last obstacle and tests if the score is correct
            int previousScore = score.Score;
            score.UpdateScore(new List<Obstacle> { new Obstacle() });
            Assert.AreEqual((int)ObstacleBehavior.BLUE * MULTIPLIER_X10 + previousScore, score.Score);

        }
    }
}
