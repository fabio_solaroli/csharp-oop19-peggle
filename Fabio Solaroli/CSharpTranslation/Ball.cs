﻿using System;

namespace CodeTranslation
{

    public interface IBall
    {
        Tuple<Double, Double> Position { get; set; }

        double Radius { get; }

        bool ReadyToLaunch { get; set; }

        bool Out { get; }

        void Update();

        void Launch();
    }

    public class Ball : IBall
    {
        private static readonly double BALL_RADIUS = WorldSettings.WORLD_HEIGHT * 0.01F;
        private bool isReadyToLaunch;

        public Ball(double radius)
        {
            this.Radius = radius;
            this.Position = Tuple.Create(0.0, 0.0);
            this.Out = false;
            this.isReadyToLaunch = false;
        }

        public Ball() : this(BALL_RADIUS)
        {
        }

        public bool ReadyToLaunch
        {
            get => this.isReadyToLaunch;
            set => this.isReadyToLaunch = value;
        }

        public bool Out { get; private set; }

        public Tuple<double, double> Position { get; set; }

        public double Radius { get; }

        public void Update()
        {
            this.Out = this.CheckIfOut();
        }

        private bool CheckIfOut()
        {
            return this.Position.Item1 <= 0;
        }

        public void Launch()
        {
        }
    }
}
