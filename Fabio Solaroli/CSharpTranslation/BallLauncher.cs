﻿using System;

namespace CodeTranslation
{

    public interface IBallLauncher
    {

        void Update(Tuple<double, double> mousePosition);

        void Load(IBall ball);

        void Launch();

        Tuple<double, double> Position { get; }

        double Width { get; }

        double Height { get; }

    }

    public class BallLauncher : IBallLauncher
    {

        private static readonly double LAUNCHER_WIDTH = WorldSettings.WORLD_HEIGHT * 0.03F;
        private static readonly double LAUNCHER_HEIGHT = WorldSettings.WORLD_HEIGHT * 0.08F;

        private static readonly double LAUNCH_IMPULSE = WorldSettings.WORLD_HEIGHT * 0.3;

        public BallLauncher()
        {
            this.Position = Tuple.Create(0.0, WorldSettings.WORLD_HEIGHT - LAUNCHER_HEIGHT);
        }

        private IBall Ball { get; set; }

        public Tuple<double, double> Position { get; private set; }

        public double Width => LAUNCHER_WIDTH;

        public double Height => LAUNCHER_HEIGHT;

        public void Update(Tuple<double, double> mousePosition)
        {
            if (mousePosition.Item1 != 0.0)
            {
                double angle = System.Math.Atan2(mousePosition.Item2, mousePosition.Item1);
                double newPositionY = WorldSettings.WORLD_HEIGHT - LAUNCHER_HEIGHT * System.Math.Sin(angle);
                double newPositionX = LAUNCHER_HEIGHT * System.Math.Cos(angle);
                Tuple<double, double> newPosition = Tuple.Create(newPositionX, newPositionY);
                this.Position = newPosition;
            }
            if (this.Ball != null && this.Ball.ReadyToLaunch)
            {
                this.Ball.Position = this.Position;
            }
        }

        public void Load(IBall ball)
        {
            this.Ball = ball;
            this.Ball.Position = this.Position;
            this.Ball.ReadyToLaunch = true;
        }

        public void Launch()
        {
            if (this.Ball != null && this.Ball.ReadyToLaunch)
            {
                this.Ball.ReadyToLaunch = false;
                this.Ball.Launch();
            }
        }
    }
}
