﻿using System;

namespace CodeTranslation
{
    public static class WorldSettings
    {
        public static readonly double WORLD_HEIGHT = 30F;

        public static readonly double WORLD_WIDTH = 30F;

    }
}
