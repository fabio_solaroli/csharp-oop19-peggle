﻿using System;
using CodeTranslation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestTranslation
{
    [TestClass]
    public class Test
    {
        private static readonly double TOLLERANCE = 0.01;

        [TestMethod]
        public void TestBallLauncherUpdate()
        {
            IBallLauncher launcher = new BallLauncher();
            Tuple<double, double> expectedPosition;
            launcher.Update(Tuple.Create(1.0, 1.0));
            expectedPosition = Tuple.Create(
                launcher.Height * Math.Sin(Math.PI / 4),
                WorldSettings.WORLD_HEIGHT - launcher.Height * Math.Cos(Math.PI / 4)
            );
            Assert.IsTrue(
                    launcher.Position.Item1 - expectedPosition.Item1 < TOLLERANCE
                    && launcher.Position.Item2 - expectedPosition.Item2 < TOLLERANCE
            );
            launcher.Update(Tuple.Create(-1.0, 1.0));
            expectedPosition = Tuple.Create(
                launcher.Height * Math.Sin(Math.PI / 4),
                WorldSettings.WORLD_HEIGHT - launcher.Height * Math.Sin(Math.PI / 4));
            Assert.IsTrue(
                    launcher.Position.Item1 - expectedPosition.Item1 < TOLLERANCE
                    && launcher.Position.Item2 - expectedPosition.Item2 < TOLLERANCE
            );
            launcher.Update(Tuple.Create(1.0, 0.0));
            expectedPosition = Tuple.Create(launcher.Height, WorldSettings.WORLD_HEIGHT);
            Assert.AreEqual(launcher.Position, expectedPosition);
            launcher.Update(Tuple.Create(-1.0, 0.0));
            expectedPosition = Tuple.Create(-launcher.Height, WorldSettings.WORLD_HEIGHT);
            Assert.AreEqual(launcher.Position, expectedPosition);
        }

        [TestMethod]
        public void TestBallLauncherLoad()
        {
            IBallLauncher launcher = new BallLauncher();
            IBall ball = new Ball();
            launcher.Load(ball);
            launcher.Update(Tuple.Create(2.0, 3.0));
            Assert.AreEqual(launcher.Position, ball.Position);
            launcher.Launch();
            Assert.IsFalse(ball.ReadyToLaunch);
        }

        [TestMethod]
        public void TestBallOut()
        {
            IBall ball = new Ball();
            ball.Position = Tuple.Create(-1.0, 0.0);
            ball.Update();
            Assert.IsTrue(ball.Out);
        }
    }
}
