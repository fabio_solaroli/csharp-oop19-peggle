﻿using System;

namespace JavaToCSharpTranslation
{
    public class CircularObstacle : Obstacle
    {
        public CircularObstacle(Tuple<double, double> position) : base(position)
        {
        }
    }
}
