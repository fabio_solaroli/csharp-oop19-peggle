﻿using System;
using System.Collections.Generic;
using System.IO;

namespace JavaToCSharpTranslation
{
    public interface IMapLoader
    {
        List<IObstacle> LoadMap(string mapName);
    }
    public class MapLoader : IMapLoader
    {
        private static readonly int NUM_GREEN_OBSTACLES = 1;
        private static readonly int NUM_PURPLE_OBSTACLES = 1;

        public List<IObstacle> LoadMap(string mapName)
        {
            List<IObstacle> obstaclesList = new List<IObstacle>();
            List<IObstacle> readyObstaclesList = new List<IObstacle>();
            Stream s = this.GetType().Assembly.GetManifestResourceStream(this.GetType(), "CSharpMaps." + mapName);

            try
            {
                StreamReader sr = new StreamReader(s);
                string line = null;

                while((line = sr.ReadLine()) != null)
                {
                    string[] obstacleParameters = line.Split(",");
                    double posX = double.Parse(obstacleParameters[1]);
                    double posY = double.Parse(obstacleParameters[2]);

                    if (obstacleParameters[0].Equals("C"))
                    {
                        obstaclesList.Add(new CircularObstacle(Tuple.Create(posX, posY)));
                    }
                    else
                    {
                        double angle = double.Parse(obstacleParameters[3]);
                        obstaclesList.Add(new RectangularObstacle(Tuple.Create(posX, posY), angle));
                    }
                }

                sr.Close();
                s.Close();
                this.SetObstaclesBehavior(obstaclesList, readyObstaclesList);
            } 
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
            return readyObstaclesList;
        }

        private void SetObstaclesBehavior(List<IObstacle> obstaclesList, List<IObstacle> readyObstaclesList)
        {
            int numRedObstacles = obstaclesList.Count % 2 == 0 ? obstaclesList.Count / 2 : (obstaclesList.Count / 2) + 1;
            this.AssignBehavior(obstaclesList, readyObstaclesList, ObstacleBehavior.RED, numRedObstacles);
            this.AssignBehavior(obstaclesList, readyObstaclesList, ObstacleBehavior.GREEN, NUM_GREEN_OBSTACLES);
            this.AssignBehavior(obstaclesList, readyObstaclesList, ObstacleBehavior.PURPLE, NUM_PURPLE_OBSTACLES);
            readyObstaclesList.AddRange(obstaclesList);
        }

        private void AssignBehavior(List<IObstacle> obstaclesList, List<IObstacle> readyObstaclesList, ObstacleBehavior behavior, int numObstacles)
        {
            if (obstaclesList.Count >= numObstacles)
            {
                for (int i = numObstacles; i > 0; i--)
                {
                    int n = new Random().Next(obstaclesList.Count);
                    IObstacle o = obstaclesList[n];
                    o.Behavior = behavior;
                    readyObstaclesList.Add(o);
                    obstaclesList.Remove(o);
                }
            }
        }
    }
}
