﻿using System;

namespace JavaToCSharpTranslation
{
    public interface IObstacle
    {
        ObstacleBehavior Behavior { get; set; }

        Tuple<double, double> Position { get;}
    }
    public class Obstacle : IObstacle
    {
        private Tuple<double, double> position;
        private ObstacleBehavior behavior;

        public Obstacle(Tuple<double, double> position)
        {
            this.position = position;
            this.behavior = ObstacleBehavior.BLUE;
        }

        public ObstacleBehavior Behavior
        {
            get => this.behavior;
            set => this.behavior = value;
        }

        public Tuple<double, double> Position
        {
            get => this.position;
        }
    }
}
