﻿namespace JavaToCSharpTranslation
{
    public enum ObstacleBehavior
    {
        RED,
        BLUE,
        GREEN,
        PURPLE
    }
}
