﻿using System;

namespace JavaToCSharpTranslation
{
    public class RectangularObstacle : Obstacle
    {
        private readonly double angle;
        public RectangularObstacle(Tuple<double, double> position, double angle) : base(position)
        {
            this.angle = angle;
        }
    }
}
