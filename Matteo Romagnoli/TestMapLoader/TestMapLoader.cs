using Microsoft.VisualStudio.TestTools.UnitTesting;
using JavaToCSharpTranslation;
using System.Collections.Generic;
using System.IO;
using System;

namespace TestMapLoader
{
    [TestClass]
    public class TestMapLoader
    {
        private static readonly int TWENTY_OBSTACLES = 20;
        private static readonly int NUM_RED_IN_TWENTY_OBSTACLES = 10;
        private static readonly int NUM_BLUE_IN_TWENTY_OBSTACLES = 8;
        private static readonly int NUM_GREEN_IN_TWENTY_OBSTACLES = 1;
        private static readonly int NUM_PURPLE_IN_TWENTY_OBSTACLES = 1;
        private static readonly int NUM_OBSTACLES_THEMISCELLANEOUS = 50;

        private int numRedObstacles;
        private int numBlueObstacles;
        private int numGreenObstacles;
        private int numPurpleObstacles;

        [TestMethod]
        public void TestZeroObstacles()
        {
            IMapLoader mapLoader = new MapLoader();
            List<IObstacle> zeroObstacles = mapLoader.LoadMap("testMap0Obstacles");
            Assert.IsTrue(zeroObstacles.Count == 0);
        }

        [TestMethod]
        public void Test20Obstacles()
        {
            IMapLoader mapLoader = new MapLoader();
            List<IObstacle> twentyObstacles = mapLoader.LoadMap("testMap20Obstacles");
            this.numRedObstacles = 0;
            this.numBlueObstacles = 0;
            this.numGreenObstacles = 0;
            this.numPurpleObstacles = 0;

            twentyObstacles.ForEach(o=> {
                if (o.Behavior == ObstacleBehavior.RED)
                {
                    this.numRedObstacles += 1;
                }
                else if (o.Behavior == ObstacleBehavior.PURPLE)
                {
                    this.numPurpleObstacles += 1;
                }
                else if (o.Behavior == ObstacleBehavior.GREEN)
                {
                    this.numGreenObstacles += 1;
                }
                else
                {
                    this.numBlueObstacles += 1;
                }
            });


            Assert.AreEqual(TWENTY_OBSTACLES, twentyObstacles.Count);
            Assert.AreEqual(NUM_BLUE_IN_TWENTY_OBSTACLES, this.numBlueObstacles);
            Assert.AreEqual(NUM_GREEN_IN_TWENTY_OBSTACLES, this.numGreenObstacles);
            Assert.AreEqual(NUM_RED_IN_TWENTY_OBSTACLES, this.numRedObstacles);
            Assert.AreEqual(NUM_PURPLE_IN_TWENTY_OBSTACLES, this.numPurpleObstacles);
        }

        [TestMethod]
        public void TestPosition()
        {
            IMapLoader mapLoader = new MapLoader();
            List<IObstacle> miscellaneousObstacles = mapLoader.LoadMap("TheMiscellaneous");
            Stream s = this.GetType().Assembly.GetManifestResourceStream(this.GetType(), "CSharpTestMaps.TheMiscellaneous");
            int obstaclesInPlace = 0;

            try
            {
                StreamReader sr = new StreamReader(s);
                string line = null;

                while ((line = sr.ReadLine()) != null)
                {
                    string[] obstacleParameters = line.Split(",");
                    double posX = double.Parse(obstacleParameters[1]);
                    double posY = double.Parse(obstacleParameters[2]);
                    Tuple<double, double> pos = new Tuple<double, double>(posX, posY);

                    miscellaneousObstacles.ForEach(o =>
                    {
                        if (o.Position.Equals(pos))
                        {
                            obstaclesInPlace += 1;
                        }
                    });
                }

                sr.Close();
                s.Close();
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }

            Assert.AreEqual(NUM_OBSTACLES_THEMISCELLANEOUS, obstaclesInPlace);
        }
    }
}
