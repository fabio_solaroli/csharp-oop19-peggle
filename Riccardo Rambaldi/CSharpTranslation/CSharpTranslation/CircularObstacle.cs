﻿using System;
namespace CSharpTranslation
{
    public class CircularObstacle : Obstacle
    {
        private static readonly double DEFAULT_RADIUS = 0.01;
        private readonly double radius = WorldSettings.HEIGHT * DEFAULT_RADIUS;

        public CircularObstacle(Tuple<double, double> position)
        {
            SetBody(position);
            measures.Add(radius);
        }

        public ObstacleType Type => ObstacleType.CIRCLE;
    }
}
