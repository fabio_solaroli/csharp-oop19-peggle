﻿using System;
using System.Collections.Generic;

namespace CSharpTranslation
{
    public interface IObstacle
    {
        ObstacleBehavior Behavior { get; }

        void SetBehavior(ObstacleBehavior color);

        Tuple<double, double> Position { get; }

        bool Hit { get; }

        void SetBody(Tuple<double, double> position);
    }

    public class Obstacle : IObstacle
    {
        protected ObstacleBehavior color;
        protected Tuple<double, double> position;
        protected bool isHit;
        protected List<double> measures;

        public Tuple<double, double> Position => position;

        public ObstacleBehavior Behavior => color;

        public void SetBehavior(ObstacleBehavior color)
        {
            this.color = color;
        }

        public bool Hit => isHit;

        public void SetBody(Tuple<double, double> position)
        {
            this.position = Tuple.Create(position.Item1, position.Item2);
            measures = new List<double>();
            color = ObstacleBehavior.BLU;
        }
    }
}
