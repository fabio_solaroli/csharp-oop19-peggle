﻿using System;
namespace CSharpTranslation
{
    public class RectangularObstacle : Obstacle
    {
        private static double HEIGHT = 0.02;
        private static double WIDTH = 0.06;
        private double height = HEIGHT * WorldSettings.WIDTH;
        private double width = WIDTH * WorldSettings.WIDTH;
        private readonly double angle;

        public RectangularObstacle(Tuple<double, double> position, double angle)
        {
            SetBody(position);
            this.angle = angle;
            measures.Add(height);
            measures.Add(width);
            measures.Add(this.angle);
        }

        public ObstacleType Type => ObstacleType.RECTANGLE;

        public double GetAngle => angle;

    }
}
