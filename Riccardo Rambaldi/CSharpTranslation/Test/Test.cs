using System;
using CSharpTranslation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class Test
    {

        [TestMethod]
        public void TestObstacleCreate()
        {
            //Create two obstacle: circle and rectangle;
            CircularObstacle circle = new CircularObstacle(Tuple.Create(0.0, 5.0));
            RectangularObstacle rect = new RectangularObstacle(Tuple.Create(0.0, 10.0), 45.0);

            Assert.AreEqual(circle.Type, ObstacleType.CIRCLE);
            Assert.AreEqual(circle.Position, Tuple.Create(0.0, 5.0));
            Assert.AreEqual(circle.Behavior, ObstacleBehavior.BLU);
            Assert.IsFalse(circle.Hit);

            Assert.AreEqual(rect.Type, ObstacleType.RECTANGLE);
            Assert.AreEqual(rect.Position, Tuple.Create(0.0, 10.0));
            Assert.AreEqual(rect.Behavior, ObstacleBehavior.BLU);
            Assert.IsFalse(rect.Hit);

        }
        [TestMethod]
        public void TestObstacleChangeColor()
        {
            CircularObstacle circle = new CircularObstacle(Tuple.Create(0.0, 5.0));
            RectangularObstacle rect = new RectangularObstacle(Tuple.Create(0.0, 10.0), 45.0);

            //Change che color of circle
            circle.SetBehavior(ObstacleBehavior.RED);
            Assert.AreEqual(circle.Behavior, ObstacleBehavior.RED);

            //Change che color of rectangle
            rect.SetBehavior(ObstacleBehavior.RED);
            Assert.AreEqual(rect.Behavior, ObstacleBehavior.RED);
        }
    }
}
